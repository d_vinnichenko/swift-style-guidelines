{\rtf1\ansi\ansicpg1251\cocoartf1671\cocoasubrtf400
{\fonttbl\f0\fnil\fcharset0 Menlo-Regular;}
{\colortbl;\red255\green255\blue255;\red87\green96\blue106;\red244\green246\blue249;\red27\green31\blue34;
\red6\green33\blue79;\red91\green40\blue180;\red203\green35\blue57;\red7\green68\blue184;}
{\*\expandedcolortbl;;\cssrgb\c41569\c45098\c49020;\cssrgb\c96471\c97255\c98039;\cssrgb\c14118\c16078\c18039;
\cssrgb\c1176\c18431\c38431;\cssrgb\c43529\c25882\c75686;\cssrgb\c84314\c22745\c28627;\cssrgb\c0\c36078\c77255;}
\paperw11900\paperh16840\margl1440\margr1440\vieww10800\viewh8400\viewkind0
\deftab720
\pard\pardeftab720\partightenfactor0

\f0\fs23\fsmilli11900 \cf2 \cb3 \expnd0\expndtw0\kerning0
# Run SwiftLint\cf4 \
START_DATE=\cf5 $(date +"%s")\cf4 \
\
SWIFT_LINT=/usr/local/bin/swiftlint\
\
\cf2 # Run SwiftLint for given filename\cf4 \
\pard\pardeftab720\partightenfactor0
\cf6 run_swiftlint\cf4 () \{\
    \cf7 local\cf4  filename=\cf5 "\cf4 $\{1\}\cf5 "\cf4 \
    \cf7 if\cf4  [[ \cf5 "\cf4 $\{filename\cf7 ##*\cf4 .\}\cf5 "\cf4  \cf7 ==\cf4  \cf5 "swift"\cf4  ]]\cf7 ;\cf4  \cf7 then\cf4 \
        $\{SWIFT_LINT\} autocorrect --path \cf5 "\cf4 $\{filename\}\cf5 "\cf4 \
        $\{SWIFT_LINT\} lint --path \cf5 "\cf4 $\{filename\}\cf5 "\cf4 \
    \cf7 fi\cf4 \
\}\
\
\cf7 if\cf4  [[ \cf7 -e\cf4  \cf5 "\cf4 $\{SWIFT_LINT\}\cf5 "\cf4  ]]\cf7 ;\cf4  \cf7 then\cf4 \
    \cf8 echo\cf4  \cf5 "SwiftLint version: $(\cf4 $\{SWIFT_LINT\}\cf5  version)"\cf4 \
    \cf2 # Run for both staged and unstaged files\cf4 \
    git diff --name-only \cf7 |\cf4  \cf7 while\cf4  \cf8 read\cf4  filename\cf7 ;\cf4  \cf7 do\cf4  run_swiftlint \cf5 "\cf4 $\{filename\}\cf5 "\cf7 ;\cf4  \cf7 done\cf4 \
    git diff --cached --name-only \cf7 |\cf4  \cf7 while\cf4  \cf8 read\cf4  filename\cf7 ;\cf4  \cf7 do\cf4  run_swiftlint \cf5 "\cf4 $\{filename\}\cf5 "\cf7 ;\cf4  \cf7 done\cf4 \
\cf7 else\cf4 \
    \cf8 echo\cf4  \cf5 "\cf4 $\{SWIFT_LINT\}\cf5  is not installed."\cf4 \
    \cf8 exit\cf4  0\
\cf7 fi\cf4 \
\
END_DATE=\cf5 $(date +"%s")\cf4 \
\
DIFF=\cf5 $((\cf4 $END_DATE\cf5  \cf7 -\cf5  \cf4 $START_DATE\cf5 ))\cf4 \
\pard\pardeftab720\partightenfactor0
\cf8 echo\cf4  \cf5 "SwiftLint took $((\cf4 $DIFF\cf5  \cf7 /\cf5  \cf8 60\cf5 )) minutes and $((\cf4 $DIFF\cf5  \cf7 %\cf5  \cf8 60\cf5 )) seconds to complete."}