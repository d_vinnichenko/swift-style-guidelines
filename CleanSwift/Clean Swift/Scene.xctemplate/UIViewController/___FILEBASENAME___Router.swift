//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

import UIKit

// Communication protocol: ViewComtroller -> Router
@objc protocol ___VARIABLE_sceneName___RoutingLogic {

    // TODO: Here need to add protocol function for router
}

// Protocol for saving data to Interactor
protocol ___VARIABLE_sceneName___DataPassing {

    // TODO: Here need to add protocol function for data passing
}

class ___VARIABLE_sceneName___Router: NSObject {

    // CleanSwift props
    weak var viewController: ___VARIABLE_sceneName___ViewController?
    var dataStore: ___VARIABLE_sceneName___DataStore?
}

// MARK: Routing Logic
extension ___VARIABLE_sceneName___Router: ___VARIABLE_sceneName___RoutingLogic {
    
    // TODO: Here need to implement protocol function for router
}

// MARK: Data Passing
extension ___VARIABLE_sceneName___Router: ___VARIABLE_sceneName___DataPassing {
    
    // TODO: Here need to implement protocol function for data passing
}
