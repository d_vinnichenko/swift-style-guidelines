//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

// Communication protocol: Interactor -> Presenter
protocol ___VARIABLE_sceneName___PresentationLogic {
    
    // TODO: Here need to add protocol function for presenter
}

class ___VARIABLE_sceneName___Presenter {
    
    // MARK: CleanSwift props
    weak var viewController: ___VARIABLE_sceneName___DisplayLogic?
}

// MARK: Presentation Logic
extension ___VARIABLE_sceneName___Presenter: ___VARIABLE_sceneName___PresentationLogic {
  
    // TODO: Here need to implement protocol function for presenter
}
