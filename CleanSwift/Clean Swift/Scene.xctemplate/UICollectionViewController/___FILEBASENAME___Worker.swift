//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

class ___VARIABLE_sceneName___Worker {

    // There should be complex buisness logic, such as working with a database.
}
