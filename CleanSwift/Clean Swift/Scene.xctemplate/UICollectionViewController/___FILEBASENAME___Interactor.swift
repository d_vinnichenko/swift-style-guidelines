//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

// Communication protocol: ViewComtroller -> Interactor
protocol ___VARIABLE_sceneName___BusinessLogic {

    // TODO: Here need to add protocol function for interactor
}

// Protocol for saving data to Interactor
protocol ___VARIABLE_sceneName___DataStore {

    // TODO: Here need to add protocol function for interactor data store
}

class ___VARIABLE_sceneName___Interactor {

    // CleanSwift props
    var presenter: ___VARIABLE_sceneName___PresentationLogic?
    var worker: ___VARIABLE_sceneName___Worker = ___VARIABLE_sceneName___Worker()
}

// MARK: Business Logic
extension ___VARIABLE_sceneName___Interactor: ___VARIABLE_sceneName___BusinessLogic {
    
    // TODO: Here need to implement protocol function for interactor
}

// MARK: Data Store
extension ___VARIABLE_sceneName___Interactor: ___VARIABLE_sceneName___DataStore {
    
    // TODO: Here need to implement protocol function for interactor data store
}
