//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

import UIKit

// Communication protocol: Presenter -> ViewComtroller
protocol ___VARIABLE_sceneName___DisplayLogic: AnyObject {
    
    // TODO: Here need to add protocol function for viewController
}

class ___VARIABLE_sceneName___ViewController: UICollectionViewController {
    
    // CleanSwift props
    var interactor: ___VARIABLE_sceneName___BusinessLogic?
    var router: (NSObjectProtocol & ___VARIABLE_sceneName___RoutingLogic & ___VARIABLE_sceneName___DataPassing)?

    override func viewDidLoad() {
        
        super.viewDidLoad()
        buildScene()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let scene = segue.identifier {
            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
            if let router = router, router.responds(to: selector) {
                router.perform(selector, with: segue)
            }
        }
    }
}

// MARK: Configuration for ___VARIABLE_sceneName___ViewController
extension ___VARIABLE_sceneName___ViewController {
    
    private func buildScene() {
        
        let interactor = ___VARIABLE_sceneName___Interactor()
        let presenter = ___VARIABLE_sceneName___Presenter()
        let router = ___VARIABLE_sceneName___Router()
        self.interactor = interactor
        self.router = router
        interactor.presenter = presenter
        presenter.viewController = self
        router.viewController = self
        router.dataStore = interactor
    }
}

// MARK: Display Logic
extension ___VARIABLE_sceneName___ViewController: ___VARIABLE_sceneName___DisplayLogic {

    // TODO: Here need to implement protocol function for viewController
}
