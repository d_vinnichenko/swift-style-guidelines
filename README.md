# EltexSoft Swift Style Guidelines

# Architecture: - CleanSwift -

### Install template generator for [CleanSwift](https://clean-swift.com) scenes.

Templates allow you to easily create a set of classes for a scene.

1) Download `CleanSwift` folder on your Mac

2) You should open Terminal and access the root directory of the folder

3) Run 

    ```
    make install_templates
    ```

# Rules: - SwiftLint -

In order to maintain the purity of the code, the identity of the approaches and the use of one style sample - at the initial stage of the project, all the steps described below must be done using the instructions below. Examples of correct and incorrect implementation can be viewed in the list of all [Rules](https://gitlab.com/d_vinnichenko/swift-style-guidelines/blob/master/Rules.md).

## Before starting the project, take the following steps.
### Install [Homebrew](http://brew.sh/) on Mac:

```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

### Install [SwiftLint](https://github.com/realm/SwiftLint) brew on Mac:

```
brew install swiftlint
```

### Setup [SwiftLint](https://github.com/realm/SwiftLint) to project (Pod):

Simply add the following line to your Podfile:

```
pod 'SwiftLint'
```

### Project settings

After that in the project you need to add settings for this library.

1) First of all, we need to add a configuration file, which already contains lists of rules and features for writing Swift code. Load the `.swiftlint.yml` file and put it in the folder with the source files of your project.

2) Add Run Script for each target in our project. 
    ```
    if which swiftlint >/dev/null; then
    swiftlint
    else
    echo "warning: SwiftLint not installed, download from https://github.com/realm/SwiftLint"
    fi
    ```

# Resources: - R.Swift -

### Setup [R.Swift](https://github.com/mac-cain13/R.swift) to project (Pod):

Simply add the following line to your Podfile:

```
pod 'R.swift'
```

### Project settings

After that in the project you need to add settings for this library.

1) In Xcode: Click on your project in the file list, choose your target under `TARGETS`, click the `Build Phases` tab and add a `New Run Script Phase` by clicking the little plus icon in the top left

2) Drag the new `Run Script` phase **above** the `Compile Sources` phase and **below** `Check Pods Manifest.lock`, expand it and paste the following script:  
   ```
   "$PODS_ROOT/R.swift/rswift" generate "$SRCROOT/R.generated.swift"
   ```

3) Add `$TEMP_DIR/rswift-lastrun` to the "Input Files" and `$SRCROOT/R.generated.swift` to the "Output Files" of the Build Phase

4) Build your project, in Finder you will now see a `R.generated.swift` in the `$SRCROOT`-folder, drag the `R.generated.swift` files into your project and **uncheck** `Copy items if needed`
